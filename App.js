import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Image,
  ActivityIndicator
} from 'react-native';
import Formulario from './components/Formulario';
import Header from './components/Header';
import axios from 'axios';
import Cotizacion from './components/Cotizacion';

const App = () => {

  const [moneda, setMoneda] = useState('');
  const [cripto, setCripto] = useState('');
  const [consultarApi, setConsultarApi] = useState(false);
  const [resultado, setResultado] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const cotizarCriptos = async () => {
      if (consultarApi) {
        // consultar api
        const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cripto}&tsyms=${moneda}`;
        const res = await axios.get(url);
        setLoading(true)
        setResultado(res.data.DISPLAY[cripto][moneda]);
        consultarApi(false);
      }
    }
    cotizarCriptos();
  }, [consultarApi]);
  
  return (
    <>
    <ScrollView>
      <Header />
      <Image
        style={styles.imagen}
        source={require('./assets/img/cryptomonedas.png')}
      />
      <View style={styles.container}>
        <Formulario
          moneda={moneda}
          cripto={cripto}
          setMoneda={setMoneda}
          setCripto={setCripto}
          setConsultarApi={setConsultarApi}
        />
      </View>
      <Cotizacion resultado={resultado} />
    </ScrollView>
    </>

  );
};

const styles = StyleSheet.create({
  imagen: {
    width: '100%',
    height: 150,
    marginHorizontal: '2.5%'
  },
  container: {
    marginHorizontal: '2.5%'
  }
});

export default App;

import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, TouchableHighlight, Alert} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import axios from 'axios';

const Formulario = ({
  moneda,
  cripto,
  setMoneda,
  setCripto,
  setConsultarApi
}) => {

  
  const [criptos, setCriptos] = useState([]);

  useEffect(() => {
    const consultarAPI = async () => {
      const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';
      const res = await axios.get(url);
      // console.log(res.data.Data);
      setCriptos(res.data.Data);
    }
    consultarAPI();
  }, [])
  

  const obtenerMoneda = moneda => {
    setMoneda(moneda);
  }
  const obtenerCriptoMoneda = cripto => {
    setCripto(cripto);
  }

  const cotizarPrecio = () => {
    if (moneda.trim() === '' || cripto.trim() === '') {
      showAlert();
      return;
    }
    setConsultarApi(true);
  }

  const showAlert = () => {
    Alert.alert(
      'Error',
      'Ambos campos son obligatorios',
      [
        {text: 'OK'}
      ]
    )
  }

  return (
    <View>
      <Text style={styles.label} >Moneda</Text>
      <Picker
        selectedValue={moneda}
        onValueChange={moneda => obtenerMoneda(moneda)}
        itemStyle={{height: 120}}
      >
        <Picker.Item label="- Seleccione -" value="" />
        <Picker.Item label="Dolar de USA" value="USD" />
        <Picker.Item label="Peso Mexicano" value="MXN" />
        <Picker.Item label="Euro" value="EUR" />
        <Picker.Item label="Libra" value="GBP" />
      </Picker>
      <Text style={styles.label} >Cripto</Text>

      <Picker
        selectedValue={cripto}
        onValueChange={cripto => obtenerCriptoMoneda(cripto)}
        itemStyle={{height: 120}}
      >
        <Picker.Item label="- Seleccione -" value="" />
        {
          criptos.map(cripto => (
            <Picker.Item key={cripto.CoinInfo.Id} label={cripto.CoinInfo.FullName} value={cripto.CoinInfo.Name} />
          ))
        }
      </Picker>

      <TouchableHighlight
        style={styles.btnCotizar}
        onPress={() => cotizarPrecio()}
      >
        <Text style={styles.textCotizar}>Cotizar</Text>
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  label: {
    fontFamily: 'Lato-Black',
    textTransform: 'uppercase',
    fontSize: 22,
    marginVertical: 20,
  },
  btnCotizar: {
    backgroundColor: '#5E49E2',
    padding: 10,
    marginTop: 20,
    borderRadius: 10,
  },
  textCotizar: {
    color: '#FFF',
    fontSize: 18,
    fontFamily: 'Lato-Black',
    textTransform: 'uppercase',
    textAlign: 'center',
  }
});

export default Formulario